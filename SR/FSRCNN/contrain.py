import argparse
from SR.core.contrain import contrain

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Continue training.')
    parser.add_argument('--model', type=str, help='path for model')
    parser.add_argument('--epoch', type=int, help='epoch', default=1000)
    parser.add_argument('--nb_epoch', type=int, help='number of epoches')

    args = parser.parse_args()
    contrain(args.model, args.nb_epoch, args.epoch)
