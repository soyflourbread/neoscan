import argparse
from SR.core.predict import predict

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Predict model.')
    parser.add_argument('--model', type=str, help='path for model')
    parser.add_argument('--image', type=str, help='path for image')
    args = parser.parse_args()
    predict(args.model, args.image, 6)
