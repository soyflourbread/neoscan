from keras.models import Sequential
from keras.layers import Conv2D, Conv2DTranspose, LeakyReLU
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, CSVLogger, TensorBoard
from .dataset import load
from .utils import psnrLoss, BLOCK_SIZE, BLOCK_NEW_SIZE

batch_size = 8
nb_epoch = 1000
channel = 3

callbacks = [ModelCheckpoint(filepath="model/model.ep{epoch:06d}.h5"), CSVLogger("model/history.csv"),
             TensorBoard(log_dir="log/")]


def train(d, s, m):
    x_train, y_train = load(mode='train')
    x_test, y_test = load(mode='validate')
    print(x_train.shape)

    model = Sequential()

    model.add(Conv2D(d, kernel_size=(5, 5), padding='valid', name='feature_extraction',
                     input_shape=(None, None, channel)))
    model.add(LeakyReLU(alpha=0.1))

    model.add(Conv2D(s, kernel_size=(1, 1), padding='valid', name='shrinking'))
    model.add(LeakyReLU(alpha=0.1))

    for x in range(0, m):
        model.add(Conv2D(s, kernel_size=(3, 3), padding='valid', name='mapping_' + str(x)))
    model.add(LeakyReLU(alpha=0.1))

    model.add(Conv2D(d, kernel_size=(1, 1), padding='valid', name='expanding'))
    model.add(LeakyReLU(alpha=0.1))

    model.add(Conv2DTranspose(
        channel, kernel_size=(9, 9), strides=(2, 2), padding='same', name='deconvolution'))

    model.compile(loss='mse',
                  optimizer=Adam(),
                  metrics=['accuracy'])

    history = model.fit(x_train, y_train,
                        batch_size=batch_size, epochs=nb_epoch,
                        verbose=1, validation_data=(x_test, y_test),
                        callbacks=callbacks, shuffle='batch')
