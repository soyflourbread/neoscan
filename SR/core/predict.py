from keras.models import load_model
import cv2
import numpy as np
from .utils import blocking, deblocking, psnrLoss, pad


def predict(model_path, image_path, padding):
    im = cv2.imread(image_path, cv2.IMREAD_COLOR)
    height, width, _ = im.shape
    im_norm = pad(np.divide(im.astype('float16'), 255), padding)

    print("Loading model.")
    model = load_model(model_path)
    print("Predicting image.")
    im_out_norm = model.predict(np.expand_dims(im_norm, axis=0))
    print("Converting imiage.")
    im_out = np.multiply(np.clip(np.squeeze(im_out_norm, axis=0), 0, 1), 255).astype('uint8')

    # cv2.imshow("GNU", im)
    # cv2.waitKey()
    # cv2.imshow("GNU", im_out)
    # cv2.waitKey()
    print("Writing out image.")
    cv2.imwrite("images/testout.png", im_out)
