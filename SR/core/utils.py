from keras import backend as K
import numpy as np

BLOCK_SIZE = 28
BLOCK_NEW_SIZE = BLOCK_SIZE * 2


def pad(tensor, length):
    return np.pad(tensor, pad_width=((length, length), (length, length), (0, 0)), mode='edge')


def log10(x):
    numerator = K.log(x)
    denominator = K.log(K.constant(10, dtype=numerator.dtype))
    return numerator / denominator


def psnrLoss(y_true, y_pred):
    return -10. * log10(K.mean(K.square(y_pred - y_true)))


def blocking(tensor, padding):
    batch = []

    height, width, _ = tensor.shape
    print("Original image:", height, "x", width)

    for h in range(0, height // BLOCK_SIZE):
        for w in range(0, width // BLOCK_SIZE):
            block = pad(tensor[h * BLOCK_SIZE: (h + 1) * BLOCK_SIZE, w * BLOCK_SIZE: (w + 1) * BLOCK_SIZE], padding)
            batch.append(block)

    return np.array(batch)


def deblocking(height, width, tensorList):
    tensor = np.ndarray(shape=(height, width, 3), dtype='uint8')
    for h in range(0, height // BLOCK_NEW_SIZE):
        for w in range(0, width // BLOCK_NEW_SIZE):
            block, tensorList = tensorList[0], tensorList[1:]
            tensor[h * BLOCK_NEW_SIZE: (h + 1) * BLOCK_NEW_SIZE,
            w * BLOCK_NEW_SIZE: (w + 1) * BLOCK_NEW_SIZE, :] = block

    return tensor


def gaussian_noise(image, stddev=0.1):
    row, col, ch = image.shape
    mean = 0
    gauss = np.random.normal(mean, stddev, (row, col, ch))
    gauss = gauss.reshape(row, col, ch)
    noisy = image + gauss
    return np.clip(noisy, 0, 1)
