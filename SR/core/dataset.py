import h5py
import os
import cv2
import random
import numpy as np
from .utils import pad, gaussian_noise, BLOCK_NEW_SIZE


def load(mode='train'):
    f = h5py.File("dataset/" + mode + ".h5", 'r')
    return f["in"], f["out"]


def gen(mode='train', padding=3):
    im_in = []
    im_out = []

    f = h5py.File("dataset/" + mode + ".h5", 'w')

    for file in os.listdir("images/" + mode):
        im = cv2.imread(os.path.join("images/" + mode, file), cv2.IMREAD_COLOR)
        height, width, _ = im.shape

        for h in range(0, height // BLOCK_NEW_SIZE):
            for w in range(0, width // BLOCK_NEW_SIZE):
                im_block = im[h * BLOCK_NEW_SIZE: (h + 1) * BLOCK_NEW_SIZE,
                           w * BLOCK_NEW_SIZE: (w + 1) * BLOCK_NEW_SIZE]
                im_block_out = np.divide(im_block.astype('float16'), 255)

                interpolation = random.randint(0, 2)
                if interpolation == 0:
                    im_resized = cv2.resize(im_block, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC)
                elif interpolation == 1:
                    im_resized = cv2.resize(im_block, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_NEAREST)
                elif interpolation == 2:
                    im_resized = cv2.resize(im_block, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
                else:
                    print("something wrong.")

                _, buf_compressed = cv2.imencode(".jpg", im_resized,
                                                 [cv2.IMWRITE_JPEG_QUALITY, random.randint(12, 20) * 5])
                im_compressed = cv2.imdecode(buf_compressed, 1)
                im_normal = np.divide(im_compressed.astype('float16'), 255)
                im_noisy = gaussian_noise(im_normal, stddev=random.uniform(0, 0.15))

                im_block_in = pad(im_noisy, padding)
                im_in.append(im_block_in)
                im_out.append(im_block_out)

    f.create_dataset("in", data=im_in)
    f.create_dataset("out", data=im_out)
    f.close()
    print(str(len(im_in)), "images are present.")
