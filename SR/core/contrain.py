from keras.callbacks import ModelCheckpoint, TensorBoard, CSVLogger
from keras.models import load_model
from .dataset import load
from .utils import psnrLoss

batch_size = 8
nb_epoch = 1000
channel = 3

callbacks = [ModelCheckpoint(filepath="model/model.ep{epoch:06d}.h5"), CSVLogger("model/history.csv"),
             TensorBoard(log_dir="log/")]


def contrain(model_path, epochCount, initialEpoch):
    print("Loading model from", model_path)
    print("Number of epoches", str(epochCount))
    print("The epoch is", str(initialEpoch))
    X_train, Y_train = load('train')
    X_test, Y_test = load('validate')
    model = load_model(model_path)
    model.fit(X_train, Y_train,
              batch_size=batch_size, epochs=epochCount, initial_epoch=initialEpoch,
              verbose=1, validation_data=(X_test, Y_test),
              callbacks=callbacks, shuffle='batch')
