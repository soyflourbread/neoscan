import json
import os
import urllib.request


def fetch_train():
    with urllib.request.urlopen("https://yande.re/post.json?api_version=2&tags=astolfo_(fate)&limit=50") as url:
        data = json.loads(url.read().decode())

        for post in data["posts"]:
            urllib.request.urlretrieve(post['file_url'],
                                       os.path.join("./images/train", str(post["id"]) + "." + post["file_ext"]))


def fetch_validate():
    with urllib.request.urlopen("https://yande.re/post.json?api_version=2&tags=fate/stay_night&limit=10") as url:
        data = json.loads(url.read().decode())

        for post in data["posts"]:
            urllib.request.urlretrieve(post['file_url'],
                                       os.path.join("./images/validate", str(post["id"]) + "." + post["file_ext"]))


if __name__ == "__main__":
    fetch_train()
    fetch_validate()
